#!/bin/bash
CI_PROJECT_ID=$(curl -sk --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects?search=Juneway_less_1" | jq '.[] | select(.name_with_namespace=="hightea / Juneway_less_1") .id');
CI_PIPELINE_ID=$(curl -sk --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines?per_page=1&page=1" | jq '.[] .id');
JOB_ID=$(curl -sk --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs" | jq '.[]| select(.name == "log") | .id');
curl -sk --location --output artifacts.zip --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/$JOB_ID/artifacts";
