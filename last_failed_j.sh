#!/bin/bash
CI_PROJECT_ID=$(curl -sk --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects?search=Juneway_less_1" | jq '.[] | select(.name_with_namespace=="hightea / Juneway_less_1") .id');
curl -sk --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs?per_page=10000&page=1&scope[]=failed" | jq '.[] | select(.name == "build_prod") .id' | head -n 1
